// App.js 

//Control de Navegacion
import React from 'react';
// https://aboutreact.com/example-of-sqlite-database-in-react-native
import 'react-native-gesture-handler';

//Importacion de librerias de navegacion
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import { createDrawerNavigator} from 'react-navigation-drawer';



// LLamados de elementos de navegacion
// Ventana de espera
import VentanaEspera from './pages/Ventana/VentanaEspera';

//Principal
import Principal from './pages/Principal';  //  Seleccion de categoria

//Navegacion
import CustomSidebarMenu from './pages/nav/Components/CustomSidebarMenu';
import NavigationDrawerHeader from './pages/nav/Components/NavigationDrawerHeader';

// Seccion ADMINISTRADOR ---------------------------------------------------------------------------------
import ProductoScreen from './pages/nav/drawerScreens/administrador/Productos';
import AsistenteScreen from './pages/nav/drawerScreens/administrador/Asistentes';
import AdminScreen from './pages/nav/drawerScreens/administrador/Admin';

//Registro Producto
import HomeScreen from './pages/nav/drawerScreens/administrador/crudproducto/HomeScreen';
import RegisterUser from './pages/nav/drawerScreens/administrador/crudproducto/RegisterUser';
import UpdateUser from './pages/nav/drawerScreens/administrador/crudproducto/UpdateUser';
import ViewUser from './pages/nav/drawerScreens/administrador/crudproducto/ViewUser';
import ViewAllUser from './pages/nav/drawerScreens/administrador/crudproducto/ViewAllUser';
import DeleteUser from './pages/nav/drawerScreens/administrador/crudproducto/DeleteUser';


// Registor de Asistentes
import ActualizarAsistente from './pages/nav/drawerScreens/administrador/crudasistente/ActualizarAsistente';
import AsistentesScreen from './pages/nav/drawerScreens/administrador/crudasistente/AsistenteScreen';
import BusquedaAsistente from './pages/nav/drawerScreens/administrador/crudasistente/BusquedaAsistente';
import DeleteAsistente from './pages/nav/drawerScreens/administrador/crudasistente/DeleteAsistente';
import ListaAsistente from './pages/nav/drawerScreens/administrador/crudasistente/ListaAsistente';
import RegistroAsistente from './pages/nav/drawerScreens/administrador/crudasistente/RegistroAsistente';


//Seccion  de USUARIO ----------------------------------------------------------------------------------------
import EncargadosScreen from './pages/nav/drawerScreens/cliente/Encargados';
import ProductoCliScreen from './pages/nav/drawerScreens/cliente/ProductoCli';
import DepayudaScreen from './pages/nav/drawerScreens/cliente/Depayuda';



// Departamentos
import asistenteA from './pages/nav/drawerScreens/cliente/departamento/asistenteA';
import asistenteB from './pages/nav/drawerScreens/cliente/departamento/asistenteB';
import asistenteC from './pages/nav/drawerScreens/cliente/departamento/asistenteC';
import asistenteD from './pages/nav/drawerScreens/cliente/departamento/asistenteD';




// Encargados independientes con sus locales respectivos

import encargadoA from './pages/nav/drawerScreens/cliente/encargados/encargadoA';
import encargadoB from './pages/nav/drawerScreens/cliente/encargados/encargadoB';
import encargadoC from './pages/nav/drawerScreens/cliente/encargados/encargadoC';
import encargadoD from './pages/nav/drawerScreens/cliente/encargados/encargadoD';
import encargadoE from './pages/nav/drawerScreens/cliente/encargados/encargadoE';
import encargadoF from './pages/nav/drawerScreens/cliente/encargados/encargadoF';


// Ubicacion
import localA from './pages/nav/drawerScreens/cliente/encargados/rutas/localA';
import localB from './pages/nav/drawerScreens/cliente/encargados/rutas/localB';
import localC from './pages/nav/drawerScreens/cliente/encargados/rutas/localC';
import localD from './pages/nav/drawerScreens/cliente/encargados/rutas/localD';
import localE from './pages/nav/drawerScreens/cliente/encargados/rutas/localE';
import localF from './pages/nav/drawerScreens/cliente/encargados/rutas/localF';

// Bienvenido tanto para usuario - administrador
import BienvenidoScreen from './pages/Bienvenidos';


// Protocolo de Bienvenido
const BienvenidoActivity_StackNavigator = createStackNavigator({
  First: {
    screen: BienvenidoScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Bienvenido Protocolo',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});



//Navegacion  de administrador     -------------------------------------------------------------------------------------
const FirstActivity_StackNavigator = createStackNavigator({
  First: {
    screen: ProductoScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Productos',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});

const SecondActivity_StackNavigator = createStackNavigator({
  First: {
    screen: AsistenteScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Asistentes',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});


const AdminActivity_StackNavigator = createStackNavigator({
  First: {
    screen: AdminScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Administracion',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});



// Modificacion de Administrador
const HomeActivity_StackNavigator = createStackNavigator({
  First: {
    screen: HomeScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Inicio Admin',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});




const RegisterUserActivity_StackNavigator = createStackNavigator({
  First: {
    screen: RegisterUser,
    navigationOptions: ({ navigation }) => ({
      title: 'Registro Admin',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});

const UpdateUserActivity_StackNavigator = createStackNavigator({
  First: {
    screen: UpdateUser,
    navigationOptions: ({ navigation }) => ({
      title: 'Actulizar Admin',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});



const ViewUserActivity_StackNavigator = createStackNavigator({
  First: {
    screen: ViewUser,
    navigationOptions: ({ navigation }) => ({
      title: 'Vista Admin',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});


const ViewAllUserActivity_StackNavigator = createStackNavigator({
  First: {
    screen: ViewAllUser,
    navigationOptions: ({ navigation }) => ({
      title: 'Vista alterada Admin',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});



const DeleteUserActivity_StackNavigator = createStackNavigator({
  First: {
    screen: DeleteUser,
    navigationOptions: ({ navigation }) => ({
      title: 'Eliminar Admin',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});



//Navegacion  de Cliente   ----------------------------------------------------------------------------------------------------
const ProductoActivity_StackNavigator= createStackNavigator({
  First: {
    screen: ProductoCliScreen, 
    navigationOptions: ({ navigation }) => ({
      title: 'Productos de la Tienda',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});

const EncargadosActivity_StackNavigator = createStackNavigator({
  First: {
    screen: EncargadosScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Encargados',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});



const DepayudaActivity_StackNavigator = createStackNavigator({
  First: {
    screen: DepayudaScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Administracion',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});

// Encargado Acciones

const encargadoAActivity_StackNavigator = createStackNavigator({
  First: {
    screen: encargadoA,
    navigationOptions: ({ navigation }) => ({
      title: 'Alejandro Mendoza',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});


const encargadoBActivity_StackNavigator = createStackNavigator({
  First: {
    screen: encargadoB,
    navigationOptions: ({ navigation }) => ({
      title: 'Alberto Guierrez',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});



const encargadoCActivity_StackNavigator = createStackNavigator({
  First: {
    screen: encargadoC,
    navigationOptions: ({ navigation }) => ({
      title: 'Alejandro Mara',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});


const encargadoDActivity_StackNavigator = createStackNavigator({
  First: {
    screen: encargadoD,
    navigationOptions: ({ navigation }) => ({
      title: 'Manolo Huarcaya',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});


const encargadoEActivity_StackNavigator = createStackNavigator({
  First: {
    screen: encargadoE,
    navigationOptions: ({ navigation }) => ({
      title: 'Administracion',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});


const encargadoFActivity_StackNavigator = createStackNavigator({
  First: {
    screen: encargadoF,
    navigationOptions: ({ navigation }) => ({
      title: 'Administracion',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});



// Rutas Ubicacion google 


const localAActivity_StackNavigator = createStackNavigator({
  First: {
    screen: localA,
    navigationOptions: ({ navigation }) => ({
      title: 'Direccion',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});


const localBActivity_StackNavigator = createStackNavigator({
  First: {
    screen: localB,
    navigationOptions: ({ navigation }) => ({
      title: 'Direccion',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});



const localCActivity_StackNavigator = createStackNavigator({
  First: {
    screen: localC,
    navigationOptions: ({ navigation }) => ({
      title: 'Direccion',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});


const localDActivity_StackNavigator = createStackNavigator({
  First: {
    screen: localD,
    navigationOptions: ({ navigation }) => ({
      title: 'Direccion',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});


const localEActivity_StackNavigator = createStackNavigator({
  First: {
    screen: localE,
    navigationOptions: ({ navigation }) => ({
      title: 'Direccion',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});


const localFActivity_StackNavigator = createStackNavigator({
  First: {
    screen: localF,
    navigationOptions: ({ navigation }) => ({
      title: 'Direccion',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});









const asistenteAActivity_StackNavigator = createStackNavigator({
  First: {
    screen: asistenteA,
    navigationOptions: ({ navigation }) => ({
      title: 'Asistente',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});

const asistenteBActivity_StackNavigator = createStackNavigator({
  First: {
    screen: asistenteB,
    navigationOptions: ({ navigation }) => ({
      title: 'Asistente',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});


const asistenteCActivity_StackNavigator = createStackNavigator({
  First: {
    screen: asistenteC,
    navigationOptions: ({ navigation }) => ({
      title: 'Asistente',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});


const asistenteDActivity_StackNavigator = createStackNavigator({
  First: {
    screen: asistenteD,
    navigationOptions: ({ navigation }) => ({
      title: 'Asistente',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});


// Crud de Aistemtes

const ListaAsistenteActivity_StackNavigator = createStackNavigator({
  First: {
    screen: ListaAsistente,
    navigationOptions: ({ navigation }) => ({
      title: 'Inicio Asistente',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});


const RegistroAsistenteActivity_StackNavigator = createStackNavigator({
  First: {
    screen: RegistroAsistente,
    navigationOptions: ({ navigation }) => ({
      title: 'Registrar Asistente',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});

const ActualizarAsistenteActivity_StackNavigator = createStackNavigator({
  First: {
    screen: ActualizarAsistente,
    navigationOptions: ({ navigation }) => ({
      title: 'Actualizar Asistente',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});


const AsistentesScreenActivity_StackNavigator = createStackNavigator({
  First: {
    screen: AsistentesScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Actualizar Asistente',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});


const BusquedaAsistenteActivity_StackNavigator = createStackNavigator({
  First: {
    screen: BusquedaAsistente,
    navigationOptions: ({ navigation }) => ({
      title: 'Actualizar Asistente',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});

const DeleteAsistenteActivity_StackNavigator = createStackNavigator({
  First: {
    screen: DeleteAsistente,
    navigationOptions: ({ navigation }) => ({
      title: 'Actualizar Asistente',
      headerLeft: ()=> <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    }),
  },
});






















// NOMBRE DEL NAVEGADOR
const DrawerNavigationRoutes = createDrawerNavigator({

  BienvenidoScreen: {
    screen: BienvenidoActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Bienvenido Screen',
    },
  },


  ProductoScreen: {
    screen: FirstActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Home Screen',
    },
  },
  AsistenteScreen: {
    screen: SecondActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Setting Screen',
    },
  },


  AdminScreen: {
    screen: AdminActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Admin Screen',
    },
  },


  HomeScreen: {
    screen: HomeActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'HomeAdmin Screen',
    },
  },

  
  RegisterUser: {
    screen: RegisterUserActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Registro Screen',
    },
  },



  UpdateUser: {
    screen: UpdateUserActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Actualizar Screen',
    },
  },

  

  ViewUser: {
    screen: ViewUserActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Vista Screen',
    },
  },

  ViewAllUser: {
    screen: ViewAllUserActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'VistaAlte Screen',
    },
  },
  

  DeleteUser: {
    screen: DeleteUserActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Eliminar Screen',
    },
  },


  ProductoCliScreen: {
    screen: ProductoActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Producto Screen',
    },
  },







  
  EncargadosScreen: {
    screen: EncargadosActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Encargado Screen',
    },
  },


  EncargadoA: {
    screen: encargadoAActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Mendoza Screen',
    },
  },



  EncargadoB: {
    screen: encargadoBActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Guierrez Screen',
    },
  },



  EncargadoC: {
    screen: encargadoCActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Mara Screen',
    },
  },



  EncargadoD: {
    screen: encargadoDActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Huarcaya Screen',
    },
  },



  EncargadoE: {
    screen: encargadoEActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Francisco Screen',
    },
  },



  EncargadoF: {
    screen: encargadoFActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Hernandez Screen',
    },
  },












  // Rutas

  
  LocalA: {
    screen: localAActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Hernandez Screen',
    },
  },


  
  LocalB: {
    screen: localBActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Hernandez Screen',
    },
  },


  
  LocalC: {
    screen: localCActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Hernandez Screen',
    },
  },


  
  LocalD: {
    screen: localDActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Hernandez Screen',
    },
  },


  
  LocalE: {
    screen: localEActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Hernandez Screen',
    },
  },


  
  LocalF: {
    screen: localFActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Hernandez Screen',
    },
  },













  AsistenteA: {
    screen: asistenteAActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Asistente Screen',
    },
  },


  
  AsistenteB: {
    screen: asistenteBActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Asistente Screen',
    },
  },


  
  AsistenteC: {
    screen: asistenteCActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Asistente Screen',
    },
  },


  
  AsistenteD: {
    screen: asistenteDActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Asistente Screen',
    },
  },








  ListaAsistente: {
  screen: ListaAsistenteActivity_StackNavigator,
  navigationOptions: {
    drawerLabel: 'Asistente Screen',
  },
},



RegistroAsistente: {
  screen: RegistroAsistenteActivity_StackNavigator,
  navigationOptions: {
    drawerLabel: 'Asistente Screen',
  },
},


ActualizarAsistente: {
  screen: ActualizarAsistenteActivity_StackNavigator,
  navigationOptions: {
    drawerLabel: 'Asistente Screen',
  },
},


AsistentesScreen: {
  screen: AsistentesScreenActivity_StackNavigator,
  navigationOptions: {
    drawerLabel: 'Asistente Screen',
  },
},


DeleteAsistente: {
  screen: DeleteAsistenteActivity_StackNavigator,
  navigationOptions: {
    drawerLabel: 'Asistente Screen',
  },
},

BusquedaAsistente: {
  screen: BusquedaAsistenteActivity_StackNavigator,
  navigationOptions: {
    drawerLabel: 'Asistente Screen',
  },
},










  DepayudaScreen: {
    screen: DepayudaActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Depayuda Screen',
    },
  },

},
{
    contentComponent: CustomSidebarMenu,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle'
});










const Auth = createStackNavigator({
  //Stack Navigator para la pantalla de inicio de sesión y registro

  LoginScreen: {
    screen: Principal,
    navigationOptions: {
      headerShown: false,
    },
  },

  /*
  HomeScreen: { screen: HomeScreen },
  SettingsScreen: { screen: SettingsScreen },  
*/
}




);

/* Cambie el Navegador para aquellas pantallas que deben cambiarse una sola vez
  y no queremos volver una vez que cambiemos de ellos a la siguiente   */
const App = createSwitchNavigator({ 
  VentanaEspera: {
    /* SplashScreen que vendrá una vez durante 2 segundos*/
    screen: VentanaEspera,
    navigationOptions: {
      /* Ocultar el encabezado de Splash Screen */
      headerShown: false,
    },
  },
  Auth: {
    /*  Auth Navigator que incluye el registro de inicio de sesión vendrá una vez */
    screen: Auth,
  },


  //Navegador
  DrawerNavigationRoutes: {
    screen: DrawerNavigationRoutes,
    navigationOptions: {
      headerShown: false,
    },
  },

});

export default createAppContainer(App);


