//Seccion de deslizamiento

import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text } from 'react-native';

const CustomSidebarMenu = props => {
  let [loginAs, setLoginAs] = useState('');

  useEffect(() => {
    setLoginAs(props.navigation.getParam('login', 'defaultValue'));
  }, []);

  // Elementos del Navegador al seleccionar el Icono  - Boton user Login Administrador

  //Problema de cambio de usuario a admin
  let itemsUser = [
    
    {
      navOptionName: 'Productos',
      screenToNavigate: 'ProductoScreen',
    },
    {
      navOptionName: 'Asistentes',
      screenToNavigate: 'AsistenteScreen',
    },
    {
      navOptionName: 'Administrador',
      screenToNavigate: 'AdminScreen',
    },
    {
      navOptionName: 'Salir',
      screenToNavigate: 'logout',
    },
  ];


    // Elementos del Navegador al seleccionar el Icono    - Boton as Loguin as Guest Usuario
  let itemsGuest = [
    {
      navOptionName: 'Productos Disponibles',
      screenToNavigate: 'ProductoCliScreen',
    },
    {
      navOptionName: 'Encargados',
      screenToNavigate: 'EncargadosScreen',
    },
    {
      navOptionName: 'Departamento Ayuda',
      screenToNavigate: 'DepayudaScreen',
    },
    {
      navOptionName: 'Salir',
      screenToNavigate: 'logout',
    },
  ];


  // Cambiar de Administrador - Usuario  de acuerdo a la opcion que elija
  const handleClick = (index, screenToNavigate) => {
    if (screenToNavigate == 'logout') {
      props.navigation.toggleDrawer();
      props.navigation.navigate('LoginScreen');
    } else if (screenToNavigate == 'ChangeOptionGuest') {
      props.navigation.toggleDrawer();
      setLoginAs('guest');
    } else if (screenToNavigate == 'ChangeOptionUser') {
      props.navigation.toggleDrawer();
      setLoginAs('user');
    } else {
      props.navigation.toggleDrawer();
      global.currentScreenIndex = screenToNavigate;
      props.navigation.navigate(screenToNavigate);
    }
  };

  // Modificar para usuario e administrador  y enlace de imagen circular
  return (
    <View style={stylesSidebar.sideMenuContainer}>
      <View style={stylesSidebar.profileHeader}>

        <View style={stylesSidebar.profileHeaderPicCircle}>
          <Text style={{ fontSize: 40, color: '#307ecc' }}>
            {'S'.charAt(0)}
          </Text>
        </View>

        <Text style={stylesSidebar.profileHeaderText}>Empresa Sodimac</Text>
      </View>
      <View style={stylesSidebar.profileHeaderLine} />

      <View style={{ width: '100%', flex: 1 }}>
        {(loginAs === 'user' ? itemsUser : itemsGuest).map((item, key) => (
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              padding: 20,
              color: 'white',
              backgroundColor:
                global.currentScreenIndex === item.screenToNavigate
                  ? '#4b9ff2'
                  : '#307ecc',
            }}
            key={key}
            onStartShouldSetResponder={() =>
              handleClick(key, item.screenToNavigate)
            }>
            <Text style={{ fontSize: 15, color: 'white' }}>
              {item.navOptionName}
            </Text>
          </View>
        ))}
      </View>
    </View>
  );
};

const stylesSidebar = StyleSheet.create({
  sideMenuContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: '#307ecc',
    paddingTop: 40,
    color: 'white',
  },
  profileHeader: {
    flexDirection: 'row',
    backgroundColor: '#307ecc',
    padding: 15,
    textAlign: 'center',
  },
  profileHeaderPicCircle: {
    width: 60,
    height: 60,
    borderRadius: 60 / 2,
    color: 'white',
    backgroundColor: '#ffffff',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  profileHeaderText: {
    fontSize: 20,
    color: 'white',
    alignSelf: 'center',
    paddingHorizontal: 20,
    fontWeight: 'bold',
  },
  profileHeaderLine: {
    height: 1,
    marginHorizontal: 20,
    backgroundColor: '#e2e2e2',
    marginTop: 15,
    marginBottom: 10,
  }
});
export default CustomSidebarMenu;