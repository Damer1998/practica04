//Productos Clientes  ProductoCliScreen

import React, { useState } from 'react';
import { Text, View, Button, SafeAreaView } from 'react-native';

// LLamado de components de administrador
import Mytextinput from '../administrador/crudproducto/components/Mytextinput';
import Mybutton from '../administrador/crudproducto/components/Mybutton';


import { openDatabase } from 'react-native-sqlite-storage';

var db = openDatabase({ name: 'UserDatabase.db' });

const ProductoCliScreen = () => {
  let [inputUserId, setInputUserId] = useState('');
  let [userData, setUserData] = useState({});

  let searchUser = () => {
    console.log(inputUserId);
    setUserData({});
    db.transaction((tx) => {
      tx.executeSql(
        'SELECT * FROM table_user where user_name = ?',
        [inputUserId],
        (tx, results) => {
          var len = results.rows.length;
          console.log('len', len);
          if (len > 0) {
            setUserData(results.rows.item(0));
          } else {
            alert('No user found');
          }
        }
      );
    });
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ flex: 1 }}>
          <Mytextinput
            placeholder="Ingrese el nombre del producto"
            onChangeText={(inputUserId) => setInputUserId(inputUserId)}
            style={{ padding: 10 }}
          />
          <Mybutton title="Iniciar Busqueda" customClick={searchUser} />
          <View style={{ marginLeft: 35, marginRight: 35, marginTop: 10 }}>
         
            <Text>Nombre de Producto: {userData.user_name}</Text>
            <Text>Sesion: {userData.user_contact}</Text>
            <Text>Disponible: {userData.user_address}</Text>
          </View>
        </View>

      </View>
    </SafeAreaView>
  );
};

export default ProductoCliScreen;