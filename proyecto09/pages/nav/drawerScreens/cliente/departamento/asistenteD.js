// Heore Ornn
import React, { Component } from 'react';

import { StyleSheet, View, Text, 
  TouchableOpacity,
  Image,  Button } from 'react-native';
  
  import Communications from 'react-native-communications';

  const imageUrl = 'https://www.signoo.es/img/cms/Instrucciones_y_consejos_conejo.png';

export default class AsistenteD extends Component {
  constructor(props) {
    super(props);
    this.state = {

      
    };
    

  }




  render() {
    const { navigate } = this.props.navigation;
    return (

      <View style={styles.listItem}  >

      <View style={styles.container}>
      <Image source={{uri:imageUrl}}  style={{width:300, height:200,borderRadius:20}} />


      <Text></Text>
               <Button 
          title="Correo"
          onPress={() => Communications.email(['aboutreact11@gmail.com', 'hello@aboutreact.com'],null,null,'Servicio Nuevo','Buenas Tardes Deseo Un servicio')}>
        </Button>

               <Text></Text>
               <Button 
          title="Mensaje"
          onPress={() => Communications.text('0123456789', 'Test Text Here')}>
        </Button>

               <Text></Text>
            <Button 
          title="LLamar"
          onPress={() => Communications.phonecall('952874274', true)}>
        </Button>

        
               <Text></Text>

   
      </View>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },
  button: {
    justifyContent: 'center',
    width : 300,
    backgroundColor:"#307cae",
    marginTop : 10,
  },
  

  titulo: {
    
    color: '#fafafa',
    fontWeight:"bold"
  },

  name: {
    flex: 0.3,
    color: '#fafafa'
  
  },



  listItem:{
    margin:12,
    padding:10,
    backgroundColor:"#b71c1c",
    width:"90%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  }
});
