// Encargados de Sectores instalacion de muebles
// EncargadosScreen

import React from 'react';
import { StyleSheet, 
         View, 
         Text ,
         Image ,
         TouchableOpacity} from 'react-native';

import { FlatGrid } from 'react-native-super-grid';


// Rutas de ubicacion
  const mapaA = 'EncargadoA';
  const mapaB = 'EncargadoB';
  const mapaC = 'EncargadoC';
  const mapaD = 'EncargadoD';
  const mapaE = 'EncargadoE';
  const mapaF = 'EncargadoF';

  


    //LLamado de Encargados
    const data =[
    { name: 'Alejandro Mendoza', 
      code: '#2980b9' , 
      enlace: mapaA,
      photo: 'http://1.bp.blogspot.com/-ryR_UYyt3Cg/Temi05QiScI/AAAAAAAAAk0/GqL0-PPvy_0/s1600/EL%2BALBA%25C3%2591IL%2BDE%2BGIRONA%252C%2BEDUARDO%2BSERRANO.JPG' },
   
    { name: 'Alberto Guierrez', 
      code: '#34495e' , 
      enlace: mapaB,
      photo: 'https://s3-eu-west-1.amazonaws.com/trabeja.com/191802/150x150_AORdbSJAJkXPqBgyNPjQr9dgposqXfCJ.jpg' },

    { name: 'Alejandro Mara ', 
      code: '#34495e' , 
      enlace: mapaC,
      photo: 'https://www.soitupro.com/public/imgs/img_profesional/18/1859.jpeg' },

    { name: 'Manolo Huarcaya', 
      code: '#2980b9' , 
      enlace: mapaD,
      photo: 'https://s3-eu-west-1.amazonaws.com/trabeja.com/54122/150x150_qm0Got3Og5kg_G8erxl5_WaVCmwTlcec.jpg'},

    { name: 'Francisco Peralta', 
      code: '#2980b9' , 
      enlace: mapaE,
      photo: 'https://s3-eu-west-1.amazonaws.com/trabeja.com/30763/150x150_SqJnd-rY9kWDZMyaqv-6cE2asVjKcfYZ.jpg'},

    { name: 'Wilmer Hernandez', 
      code: '#34495e' , 
      enlace: mapaF,
      photo: 'http://4.bp.blogspot.com/-wzsv1Ywhafk/UQFKD7BpHRI/AAAAAAAAFyo/FRkA5kivumQ/s238/EL%2BALBA%25C3%2591IL%2BXAVIER%2BVALDERAS.JPG'},

    
  ];

  export default class EncargadosScreen extends React.Component {

  // Se llama el const de data
  constructor(props) {
    super(props);
    this.state = {
      data,
    };
  }

  render(){
    const { navigate } = this.props.navigation;
  return (
    <View style={styles.container}>
    
    <FlatGrid
      itemDimension={130}
      data={this.state.data}

      style={styles.gridView}
      // staticDimension={300}
      // fixed
      spacing={10}
      renderItem={({ item }) => 


        <TouchableOpacity style={styles.listItem} 
    
        onPress={() => navigate(item.enlace)} >


        <View style={[styles.itemContainer, { backgroundColor: item.code }]}>
        
        <Image source={{uri:item.photo}}  style={{width:110, height:110,borderRadius:50}} />

          <Text style={styles.itemName}>{item.name}</Text>
  
        </View>

        </TouchableOpacity>


      }
    />

</View>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
 

  },
  gridView: {
    marginTop: 10,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '800',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});