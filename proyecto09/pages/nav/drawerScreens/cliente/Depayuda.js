//Departamentos de Quejas

import React from 'react';
import { StyleSheet, 
         View, 
         Text ,
         Image ,
         TouchableOpacity} from 'react-native';

import { FlatGrid } from 'react-native-super-grid';


// Rutas de ubicacion
  const mapaA = 'AsistenteA';
  const mapaB = 'AsistenteB';
  const mapaC = 'AsistenteC';
  const mapaD = 'AsistenteD';

  


    //LLamado de Encargados
    const data =[
    { name: 'Asistente de Devoluciones', 
      code: '#2980b9' , 
      enlace: mapaA,
      photo: 'https://cdn.icon-icons.com/icons2/876/PNG/512/user-on-round-button_icon-icons.com_68280.png' },
   
    { name: 'Asistente de Informes', 
      code: 'gray' , 
      enlace: mapaB,
      photo: 'https://cdn.icon-icons.com/icons2/876/PNG/512/user-on-round-button_icon-icons.com_68280.png' },

    { name: 'Asistente de Reclamos ', 
      code: 'gray' , 
      enlace: mapaC,
      photo: 'https://cdn.icon-icons.com/icons2/876/PNG/512/user-on-round-button_icon-icons.com_68280.png' },

    { name: 'Asistente de Bancario', 
      code: '#2980b9' , 
      enlace: mapaD,
      photo: 'https://cdn.icon-icons.com/icons2/876/PNG/512/user-on-round-button_icon-icons.com_68280.png'},

    
  ];

  export default class DepayudaScreen extends React.Component {

  // Se llama el const de data
  constructor(props) {
    super(props);
    this.state = {
      data,
    };
  }

  render(){
    const { navigate } = this.props.navigation;
  return (
    <View style={styles.container}>
    
    <FlatGrid
      itemDimension={130}
      data={this.state.data}

      style={styles.gridView}
      // staticDimension={300}
      // fixed
      spacing={10}
      renderItem={({ item }) => 


        <TouchableOpacity style={styles.listItem} 
    
        onPress={() => navigate(item.enlace)} >


        <View style={[styles.itemContainer, { backgroundColor: item.code }]}>
        
        <Image source={{uri:item.photo}}  style={{width:90, height:90,borderRadius:50}} />

          <Text style={styles.itemName}>{item.name}</Text>
  
        </View>

        </TouchableOpacity>


      }
    />

</View>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
 

  },
  gridView: {
    marginTop: 80,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '800',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});