// Eliminar  asitentes

// table_user cambiar

import React, { useState } from 'react';
import { Button, Text, View, Alert, SafeAreaView } from 'react-native';

// LLamado de components de administrador
import Mytextinput from './componentsAsistente/Mitextinput';
import Mybutton from './componentsAsistente/Miboton';


import { openDatabase } from 'react-native-sqlite-storage';

var db = openDatabase({ name: 'UserDatabase.db' });

const DeleteAsistente = ({ navigation }) => {
  let [inputUserId, setInputUserId] = useState('');

  let DeleteAsistente = () => {
    db.transaction((tx) => {
      tx.executeSql(
        'DELETE FROM  table_asistente where user_id=?',
        [inputUserId],
        (tx, results) => {
          console.log('Results', results.rowsAffected);
          if (results.rowsAffected > 0) {
            Alert.alert(
              'Success',
              'Producto Eliminado',
              [
                {
                  text: 'Ok',
                  onPress: () => navigation.navigate('AsistenteScreen'),
                },
              ],
              { cancelable: false }
            );
          } else {
            alert('Por favor insertar el Id para validar');
          }
        }
      );
    });
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ flex: 1 }}>
          <Mytextinput
            placeholder="Ingrese el id correspondiente"
            onChangeText={(inputUserId) => setInputUserId(inputUserId)}
            style={{ padding: 10 }}
          />
          <Mybutton title="Eliminar Producto" customClick={DeleteAsistente} />
        </View>

      </View>
    </SafeAreaView>
  );
};

export default DeleteAsistente;