// Lista de usuario admnistrador Producto 

import React, { useState } from 'react';
import { Text, View, Button, SafeAreaView } from 'react-native';

// LLamado de components de administrador
import Mytextinput from './componentsAsistente/Mitextinput';
import Mybutton from './componentsAsistente/Miboton';


import { openDatabase } from 'react-native-sqlite-storage';

var db = openDatabase({ name: 'UserDatabase.db' });

const BusquedaAsistente = () => {
  let [inputUserId, setInputUserId] = useState('');
  let [userData, setUserData] = useState({});

  let searchUser = () => {
    console.log(inputUserId);
    setUserData({});
    db.transaction((tx) => {
      tx.executeSql(
        'SELECT * FROM table_asistente where user_id = ?',
        [inputUserId],
        (tx, results) => {
          var len = results.rows.length;
          console.log('len', len);
          if (len > 0) {
            setUserData(results.rows.item(0));
          } else {
            alert('Asistente No Encontrado');
          }
        }
      );
    });
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ flex: 1 }}>
          <Mytextinput
            placeholder="Ingresar Asistente Id"
            onChangeText={(inputUserId) => setInputUserId(inputUserId)}
            style={{ padding: 10 }}
          />
          <Mybutton title="Busca Asistente" customClick={searchUser} />
          <View style={{ marginLeft: 35, marginRight: 35, marginTop: 10 }}>
            <Text>User Id: {userData.user_id}</Text>
            <Text>User Nombre: {userData.user_name}</Text>
            <Text>User Area: {userData.user_contact}</Text>
            <Text>User Turno: {userData.user_address}</Text>
          </View>
        </View>

      </View>
    </SafeAreaView>
  );
};

export default BusquedaAsistente;