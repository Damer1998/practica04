// Home screen producto  AsistenteScreen-- Asistent

import React, { useEffect } from 'react';
import { View, Text, SafeAreaView } from 'react-native';

// LLamado de components de administrador  table_asistente

import Mybutton from './componentsAsistente/Miboton';
import Mytext  from './componentsAsistente/Mitexto';


import { openDatabase } from 'react-native-sqlite-storage';

var db = openDatabase({ name: 'UserDatabase.db' });

const AsistenteScreen = ({ navigation }) => {
  useEffect(() => {
    db.transaction(function (txn) {
      txn.executeSql(
        "SELECT name FROM sqlite_master WHERE type='table' AND name='table_asistente'",
        [],
        function (tx, res) {
          console.log('item:', res.rows.length);
          if (res.rows.length == 0) {
            txn.executeSql('DROP TABLE IF EXISTS table_asistente', []);
            txn.executeSql(
              'CREATE TABLE IF NOT EXISTS table_asistente(user_id INTEGER PRIMARY KEY AUTOINCREMENT, user_name VARCHAR(20), user_contact INT(10), user_address VARCHAR(255))',
              []
            );
          }
        }
      );
    });
  }, []);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ flex: 1 }}>
          <Mytext text="Seccion de Envio de Asistente" />
          <Mybutton
            title="Registrar Asistente"
            customClick={() => navigation.navigate('RegistroAsistente')}
          />
          <Mybutton
            title="Actualizar Asistente"
            customClick={() => navigation.navigate('ActualizarAsistente')}
          />
          <Mybutton
            title="Busqueda de Asistente"
            customClick={() => navigation.navigate('BusquedaAsistente')}
          />
          <Mybutton
            title="Lista de Asistente"
            customClick={() => navigation.navigate('ListaAsistente')}
          />
          <Mybutton
            title="Eliminar Asistente"
            customClick={() => navigation.navigate('DeleteAsistente')}
          />
        </View>

      </View>
    </SafeAreaView>
  );
};

export default AsistenteScreen;