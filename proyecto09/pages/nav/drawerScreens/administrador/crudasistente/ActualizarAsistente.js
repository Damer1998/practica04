//Actualizar Producto ADMINISTRADOR
import React, { useState } from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  SafeAreaView,
  Text,
} from 'react-native';

// LLamado de components de administrador

import Mytextinput from './componentsAsistente/Mitextinput';
import Mybutton from './componentsAsistente/Miboton';

import { openDatabase } from 'react-native-sqlite-storage';

var db = openDatabase({ name: 'UserDatabase.db' });

const ActualizarAsistente = ({ navigation }) => {
  let [inputUserId, setInputUserId] = useState('');
  let [userName, setUserName] = useState('');
  let [userContact, setUserContact] = useState('');
  let [userAddress, setUserAddress] = useState('');

  let updateAllStates = (name, contact, address) => {
    setUserName(name);
    setUserContact(contact);
    setUserAddress(address);
  };

  let searchUser = () => {
    console.log(inputUserId);
    db.transaction((tx) => {
      tx.executeSql(
        'SELECT * FROM table_asistente where user_id = ?',
        [inputUserId],
        (tx, results) => {
          var len = results.rows.length;
          if (len > 0) {
            let res = results.rows.item(0);
            updateAllStates(res.user_name, res.user_contact, res.user_address);
          } else {
            alert('Asistente servicio denegado');
            updateAllStates('', '', '');
          }
        }
      );
    });
  };
  let updateUser = () => {
    console.log(inputUserId, userName, userContact, userAddress);

    if (!inputUserId) {
      alert('Por favor rellene el ID del Asistente');
      return;
    }
    if (!userName) {
      alert('Por favor rellene el nombre del Asistente');
      return;
    }
    if (!userContact) {
      alert('Por favor rellene el Area del Asistente');
      return;
    }
    if (!userAddress) {
      alert('Por favor rellene Turno del Asistente');
      return;
    }

    db.transaction((tx) => {
      tx.executeSql(
        'UPDATE table_asistente set user_name=?, user_contact=? , user_address=? where user_id=?',
        [userName, userContact, userAddress, inputUserId],
        (tx, results) => {
          console.log('Results', results.rowsAffected);
          if (results.rowsAffected > 0) {
            Alert.alert(
              'Success',
              'Asistente Actualizado',
              [
                {
                  text: 'Ok',
                  onPress: () => navigation.navigate('AsistenteScreen'),
                },
              ],
              { cancelable: false }
            );
          } else alert('Actualizacion Fallida');
        }
      );
    });
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ flex: 1 }}>
          <ScrollView keyboardShouldPersistTaps="handled">
            <KeyboardAvoidingView
              behavior="padding"
              style={{ flex: 1, justifyContent: 'space-between' }}>
              <Mytextinput
                placeholder="Ingresar ID"
                style={{ padding: 10 }}
                onChangeText={(inputUserId) => setInputUserId(inputUserId)}
              />
              <Mybutton title="Buscar Producto" customClick={searchUser} />
              <Mytextinput
                placeholder="Ingresar Producto"
                value={userName}
                style={{ padding: 10 }}
                onChangeText={(userName) => setUserName(userName)}
              />
              <Mytextinput
                placeholder="Ingresar Area Correspondiente"
                value={'' + userContact}
                onChangeText={(userContact) => setUserContact(userContact)}
                maxLength={10}
                style={{ padding: 10 }}
                keyboardType="numeric"
              />
              <Mytextinput
                value={userAddress}
                placeholder="Tipo"
                onChangeText={(userAddress) => setUserAddress(userAddress)}
                maxLength={225}
                numberOfLines={5}
                multiline={true}
                style={{ textAlignVertical: 'top', padding: 10 }}
              />
              <Mybutton title="Actualizar Producto" customClick={updateUser} />
            </KeyboardAvoidingView>
          </ScrollView>
        </View>

      </View>
    </SafeAreaView>
  );
};

export default ActualizarAsistente;