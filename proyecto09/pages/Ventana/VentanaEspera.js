// Primer Splash
//Importacion de read Y hook
import React, { useState, useEffect } from 'react';

//Importacion de requerimientos
import { ActivityIndicator, View, StyleSheet, Image } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const SplashScreen = props => {
  //State for ActivityIndicator animation
  let [animating, setAnimating] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setAnimating(false);
      //Compruebe si user_id está configurado o no
      //Si no es así, envíe para la autenticación
      //  otro enviar a la pantalla de HomeScreen
      AsyncStorage.getItem('user_id').then(value =>
        props.navigation.navigate(
          value === null ? 'Auth' : 'Principal'  // Ver Cambios 
        )
      );
    }, 2000);  // Timepo
  }, []);

  return (
    <View style={styles.container}>
      <Image
        source={require('../Image/logo.png')}
        style={{ width: '90%', resizeMode: 'contain', margin: 30 }}
      />
      <ActivityIndicator
        animating={animating}
        color="#1a237e"
        size="large"
        style={styles.activityIndicator}
      />
    </View>
  );
};
export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#e8eaf6',
  },
  activityIndicator: {
    alignItems: 'center',
    height: 80,
  },
});