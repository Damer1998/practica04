// Area de Logueo
import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';

// Creating Login Screen
const Principal = props => {
  return (
    <View style={styles.mainBody}>
      <View style={{ alignItems: 'center' }}>
         <Image
          source={require('./Image/logo.png')}
          style={{
            width: '80%',
            height: 200,
            resizeMode: 'contain',
            margin: 30,
          }}
        /> 
        <Text
          style={{
            fontSize: 40,
            color: '#307ecc',
            fontWeight: 'bold',
            paddingVertical: 20,
          }}>
          Bienvenidos
        </Text>
      </View>
      <TouchableOpacity
        style={styles.buttonStyle}
        activeOpacity={0.5}
        onPress={() =>
          props.navigation.navigate('DrawerNavigationRoutes', { login: 'user' })
        }>
        <Text style={styles.buttonTextStyle}>Administrador</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.buttonStyle}
        activeOpacity={0.5}
        onPress={() =>
          props.navigation.navigate('DrawerNavigationRoutes', {
            login: 'guest',
          })
        }>
        <Text style={styles.buttonTextStyle}>Cliente</Text>
      </TouchableOpacity>
    </View>
  );
};
export default Principal;

const styles = StyleSheet.create({
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  buttonStyle: {
    backgroundColor: '#7DE24E',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#7DE24E',
    height: 40,
    alignItems: 'center',
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 20,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    paddingVertical: 10,
    fontSize: 16,
  },
});