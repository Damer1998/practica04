import React from 'react';
import { StyleSheet, View, Text ,Image} from 'react-native';
import { FlatGrid } from 'react-native-super-grid';




export default function BienvenidoScreen() {

    //LLamado de elementos
  const [items, setItems] = React.useState([
    { name: 'Lavase las manos', code: '#1abc9c' , photo: 'https://ratiopharm.es/sites/default/files/styles/post_main_image/public/2019-10/Concienciar%20desde%20la%20farmacia%20sobre%20el%20lavado%20de%20manos%20%C2%BFpor%20qu%C3%A9%20es%20tan%20importante.jpg?itok=iXkHFPXk' },
    { name: 'Usar Gel antibacterial', code: '#2ecc71' , photo: 'http://quimiform.com/wp-content/uploads/2020/04/gel-antibacterial.jpg' },
    { name: 'Usar Mascarilla', code: '#3498db', photo: 'https://cdn.pixabay.com/photo/2020/03/30/11/00/mask-4983883_1280.png' },
    { name: 'Lavar los productos', code: '#9b59b6' , photo: 'https://image.freepik.com/vector-gratis/serie-diseno-carteles-lavado-frutas_7096-187.jpg'},
    { name: 'Ducharse al regresar a su hogar', code: '#34495e' , photo: 'https://image.freepik.com/vector-gratis/ejemplo-bano-ducha-muchacho-nino_97632-172.jpg'},
    { name: 'LLamar 1517 o 1540', code: '#2980b9' , photo: 'https://st2.depositphotos.com/3616015/8122/v/950/depositphotos_81220232-stock-illustration-building-hospital-flat-icon-with.jpg'},


  ]);

  return (
    <FlatGrid
      itemDimension={130}
      data={items}
      style={styles.gridView}
      // staticDimension={300}
      // fixed
      spacing={10}
      renderItem={({ item }) => (
        <View style={[styles.itemContainer, { backgroundColor: item.code }]}>
        
        <Image source={{uri:item.photo}}  style={{width:90, height:90,borderRadius:40}} />

          <Text style={styles.itemName}>{item.name}</Text>
  
        </View>
      )}
    />
  );
}

const styles = StyleSheet.create({
  gridView: {
    marginTop: 10,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});